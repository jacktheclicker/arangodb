
@startDocuBlock JSF_cluster_test_HEAD
@brief executes a cluster roundtrip for sharding

@RESTHEADER{HEAD /_admin/cluster-test, Execute cluster roundtrip}

@RESTDESCRIPTION
See GET method.

@RESTRETURNCODES

@RESTRETURNCODE{200} is returned when everything went well.

@endDocuBlock

