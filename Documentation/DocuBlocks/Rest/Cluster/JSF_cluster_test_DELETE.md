
@startDocuBlock JSF_cluster_test_DELETE
@brief executes a cluster roundtrip for sharding

@RESTHEADER{DELETE /_admin/cluster-test, Delete cluster roundtrip}

@RESTDESCRIPTION
See GET method.

@RESTRETURNCODES

@RESTRETURNCODE{200} is returned when everything went well.

@endDocuBlock

