Cluster
=======

- [Mesos, DC/OS](Mesos.md):
  Distributed deployment using Apache Mesos

- [Generic & Docker](ArangoDBStarter.md):
  Automatic native clusters with ArangoDB Starter

- [Advanced Topics](Advanced.md):
  Standalone Agency, local / distributed / Docker clusters
