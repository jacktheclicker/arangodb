Advanced Topics
---------------

In contrast to the other topics in this chapter that strive to get you simply set up in prepared environments, The following chapters describe whats going on under the hood in details, the components of ArangoDB Clusters, and how they're put together:

- [Standalone Agency](Agency.md)
- [Test setup on a local machine](Local.md)
- [Starting processes on different machines](Distributed.md)
- [Launching an ArangoDB cluster using Docker containers](Docker.md)
